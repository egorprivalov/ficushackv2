//
//  ATOYError.swift
//  Smartfeed
//
//  Created by Sergey Garazha on 10/16/16.
//  Copyright © 2016 Smartfeed. All rights reserved.
//

import Foundation

let kUnauthorizedDefaultTitle = "Hi!"
let kUnauthorizedDefaultMessage = "Hi! Sign up with an email and password and you're ready to roll."
let kUnauthorizedReviewMessage = "Hi! An email + password signs you up. Thanks!"
let kUnauthorizedErrorNotificationName = Notification.Name("unauthorized_response")

public enum FicusError: Error {
  case unsuccessful
  case failureResponse(_ : Int?)
  case badJSON
  case parsingError(_ :String)
  case unknown
  case custom(_: String)
  case unathorized(_: String?)
  case silent
  case jwtTokenValid
  
  var localizedDescriptionError: String {
    switch self {
    case .unsuccessful: return "Failure response"
    case .failureResponse(let code): return "Failure response (\(code ?? 0))"
    case .badJSON: return "Bad response format"
    case .parsingError(let fieldName): return "Reponse parsing failed (\(fieldName))"
    case .custom(let str): return str
    case .jwtTokenValid: return "jwtToken is valid"
    case .unathorized(let message):
      return message ?? kUnauthorizedDefaultMessage
    default: return "Unknown"
    }
  }
  
  var code: Int? {
    switch self {
    case .failureResponse(let responseCode): return responseCode
    default: return nil
    }
  }
}
