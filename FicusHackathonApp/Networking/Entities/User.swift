//
//  User.swift
//  ATOY
//
//  Created by  on 09.12.16.
//
//

import Foundation
import SwiftyJSON

enum Role: String {
  case customer = "customer"
  case provider = "provider"
}

public class User: Arrayable {
  var id: Int!
  var googleId: String?
  var facebookId: String?
  var email: String!
  var isAdmin: Bool!
  var name: String!
  var role = Role.customer
  
  required public init(_ json: JSON) {
    
    if let id = json["id"].int { self.id = id }
    
    if let email = json["email"].string { self.email = email }
    
    if let isAdmin = json["is_admin"].bool { self.isAdmin = isAdmin }
    
    if let name = json["name"].string { self.name = name }
    
    
  }
}
