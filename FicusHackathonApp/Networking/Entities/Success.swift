//
//  Success.swift
//  Cave2
//
//  Created by Anton Pavlov on 20/02/2019.
//  Copyright © 2019 Anton Pavlov. All rights reserved.
//

import Foundation
import SwiftyJSON


class Success {
  
  var success: Bool!
  var message: String?
  
  init(_ json: JSON) {
    
    if let success = json["success"].bool {
      self.success = success
    }
    
    if let message = json["message"].string {
      self.message = message
    }
  }
}
