//
//  UserFicus.swift
//  Ficus
//
//  Created by Egor Privalov on 08.06.2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import Foundation
import SwiftyJSON

public class UserFicus: Arrayable {
    
    var login: String?
    var extID: String?
    var phoneNumber: String?
    var hasCarLicense: Bool?
    var imageUrl: String?
    var firstName: String?
    var secondName: String?
    var sex: Int?
    var eMail: String?
    var password: String?
    
    required public init(_ json: JSON) {
        if let login = json["Login"].string { self.login = login }
        if let extID = json["ExtID"].string { self.extID = extID }
        if let phoneNumber = json["PhoneNumber"].string { self.phoneNumber = phoneNumber }
        if let hasCarLicense = json["HasCarLicense"].bool { self.hasCarLicense = hasCarLicense }
        if let imageUrl = json["ImageUrl"].string { self.imageUrl = imageUrl }
        if let firstName = json["FirstName"].string { self.firstName = firstName }
        if let secondName = json["SecondName"].string { self.secondName = secondName }
        if let sex = json["Sex"].int { self.sex = sex }
        if let eMail = json["EMail"].string { self.eMail = eMail }
        if let password = json["Password"].string { self.password = password }
    }
    
    var gender: String? {
        switch sex {
        case 1: return "Мужской"
        case 2: return "Женский"
        default: return "Трансмутация"
        }
    }
}
