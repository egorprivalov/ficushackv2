//
//  Settings.swift
//  Rightangled
//
//  Created by  on 14.12.16.
//
//

import Foundation
import RxAlamofire
import Alamofire
import SwiftyJSON
//import FBSDKCoreKit
//import FBSDKLoginKit
//import GoogleSignIn
import UIKit
import RxSwift
import RxCocoa

class Settings  {
    
    let db = DisposeBag()
    
    init() {
        
    }
    
    //  var lastTimeRefreshToken: Date?
    
    static var lastTimeRefreshToken: String? {
        set {
            defaults.set(newValue, forKey: "lastTimeRefreshToken")
            defaults.synchronize()
        }
        get {
            return defaults.string(forKey: "lastTimeRefreshToken")
        }
    }
    
    
    var user: UserFicus?
    
    var data: Observable<DataRequest>?
    
    var isShowAddUser = false
    var isShowAddUserSignOut = false
    var isAppLoad = false
    
    static let defaults = UserDefaults.standard
    
    static var email: String? {
        set {
            defaults.set(newValue, forKey: "email")
            defaults.synchronize()
        }
        get {
            return defaults.string(forKey: "email")
        }
    }
    
    static var password: String? {
        get {
            return defaults.string(forKey: "pass")
        }
        set {
            defaults.set(newValue, forKey: "pass")
            defaults.synchronize()
        }
    }
    
    static var phoneNumber: String? {
        set {
            defaults.set(newValue, forKey: "phoneNumber")
            defaults.synchronize()
        }
        get {
            return defaults.string(forKey: "phoneNumber")
        }
    }
    
    static var jwtToken: String? {
        set {
            defaults.set(newValue, forKey: "jwtToken")
            defaults.synchronize()
        }
        get {
            return defaults.string(forKey: "jwtToken")
        }
    }
    
    static var refreshToken: String? {
        set {
            defaults.set(newValue, forKey: "refreshToken")
            defaults.synchronize()
        }
        get {
            return defaults.string(forKey: "refreshToken")
        }
    }
    
    static var userGuid: String? {
        set {
            defaults.set(newValue, forKey: "userGuid")
            defaults.synchronize()
        }
        get {
            return defaults.string(forKey: "userGuid")
        }
    }
    
    static var fbUserID: String? {
        set {
            defaults.set(newValue, forKey: "fbuserid")
            defaults.synchronize()
        }
        get {
            return defaults.string(forKey: "fbuserid")
        }
    }
    
    static var fbToken: String? {
        set {
            defaults.set(newValue, forKey: "fbtoken")
            defaults.synchronize()
        }
        get {
            return defaults.string(forKey: "fbtoken")
        }
    }
    
    static var ggUserID: String? {
        set {
            defaults.set(newValue, forKey: "gguserid")
            defaults.synchronize()
        }
        get {
            return defaults.string(forKey: "gguserid")
        }
    }
    
    static var ggToken: String? {
        set {
            defaults.set(newValue, forKey: "ggtoken")
            defaults.synchronize()
        }
        get {
            return defaults.string(forKey: "ggtoken")
        }
    }
    
    static var isUserLoggedIn: Bool {
        set {
            defaults.set(newValue, forKey: "isuserLoggedIn")
            defaults.synchronize()
        }
        get {
            return defaults.bool(forKey: "isuserLoggedIn")
        }
    }
    
    static var isFirstTime: Bool {
        set {
            defaults.set(newValue, forKey: "isFirstTime")
            defaults.synchronize()
        }
        get {
            return defaults.bool(forKey: "isFirstTime")
        }
    }
    
    //    static func getService(completed: (([ServiceCategory])->())? = nil) {
    //        let db = DisposeBag()
    //
    //        if Settings.shared.serviceCategories.isEmpty {
    //            ContentNetworkManager.getServiceCategories().subscribe(onNext: { categories in
    //                Settings.shared.serviceCategories = categories
    //                completed?(categories)
    //            }, onError: { error in
    //            }).disposed(by: db)
    //        } else {
    //            completed?(Settings.shared.serviceCategories)
    //        }
    //    }
    
    //Notifications
    static var pushToken: Data?
    static var pushTokenString: String?
}

extension Settings {
    
    static func logout() {
        email = nil
        password = nil
        phoneNumber = nil
        fbToken = nil
        fbUserID = nil
        ggToken = nil
        ggUserID = nil
        isUserLoggedIn = false
        
        jwtToken = nil
        refreshToken = nil
        userGuid = nil
        
        //    GIDSignIn.sharedInstance().signOut()
        
        let vc = SignInViewController()
        UIApplication.shared.keyWindow?.rootViewController = vc
        //user = nil
        
        // Google
        
        // Facebook
        //        FBSDKLoginManager().logOut()
        //        FBSDKAccessToken.setCurrent(nil)
        
    }
    
}
