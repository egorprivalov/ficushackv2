//
//  UploadManager.swift
//  Rightangled
//
//  Created by Anton Pavlov on 09/12/2018.
//  Copyright © 2018 Anton Pavlov. All rights reserved.
//

import UIKit
import RxSwift
import Alamofire
import SwiftyJSON


fileprivate enum API {
    static let mandatoryPart = ContentNetworkManager.serverURL + ContentNetworkManager.pathPrefix
    
    static func request(_ id: Int) -> String {return mandatoryPart + "/requests/update_map/\(id)"}
    static func video(_ requestID: Int, _ serviceItemID: Int) -> String {return mandatoryPart + "/requests/\(requestID)/upload_video/\(serviceItemID)"}
    static func audio(_ requestID: Int, _ serviceItemID: Int) -> String {return mandatoryPart + "/requests/\(requestID)/upload_audio/\(serviceItemID)"}
}

enum AttachmentType: String {
    case audio = "audio_file"
    case image = "image"
    case video = "video_file"
    case none = "none"
    
    var uploadTypeString: String {
        switch self {
        case .image: return "image/jpeg"
        case .audio: return "audio/x-caf"
        case .video: return "video/quicktime"
        default: return ""
        }
    }
    
    var sExtension: String {
        switch self {
        case .image: return ".jpg"
        case .audio: return ".mp3"
        case .video: return ".mp4"
        default: return ""
        }
    }
    
    func api(_ requestID: Int, _ serviceItemID: Int) -> String{
        switch self {
        case .audio: return API.audio(requestID, serviceItemID)
        case .video: return API.video(requestID, serviceItemID)
        default: return ""
        }
    }
    
}

class UploadManager {
    
    static let shared = UploadManager()
    
//    func uploadImage(_ room: Room, data: Data) -> Observable<Room> {
//        return Observable.create({ (observer) -> Disposable in
//
//            Alamofire.upload(multipartFormData: { multipartFormData in
//                let time = Date().timeIntervalSince1970
//                let timeInt = Int(time) / 2
//                multipartFormData.append(data, withName: "image", fileName: "file\(room.id!)/\(timeInt).jpg", mimeType: "image/jpg")
//                for (key, value) in room.toJSON {
//
//                    if value is String {
//                        multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
//                    }
//
//                }
//
//            }, to: API.rooms(room.id), method: .put, headers: nil, encodingCompletion: { (result) in
//                switch result {
//                case .success(let upload, _, _):
//
//                    upload.uploadProgress(closure: { (progress) in
//                        print("Upload Progress: \(progress.fractionCompleted)")
//                    })
//
//                    upload.responseJSON { response in
//                        if let value = response.result.value {
//                            observer.onNext(Room(JSON(value)))
//                            observer.onCompleted()
//                        } else if let error = response.result.error {
//                            observer.onError(error)
//                        }
//
//                    }
//
//                case .failure(let encodingError):
//                    observer.onError(encodingError)
//                }
//            })
//
//            return Disposables.create()
//        })
//
//    }
    
//    func upload(_ item: ServiceItem, type: AttachmentType, data: Data) -> Observable<ServiceItem> {
//
//        guard let requestId = item.requestId, let id = item.id else {
//            return Observable<ServiceItem>.error(CaveError.custom("id nil"))
//        }
//
//        return Observable.create({ (observer) -> Disposable in
//
//            Alamofire.upload(multipartFormData: { multipartFormData in
//                multipartFormData.append(data, withName: type.rawValue, fileName: "file" + type.sExtension, mimeType: type.uploadTypeString)
//
//            }, to: type.api(requestId, id), method: .post, headers: nil, encodingCompletion: { result in
//                switch result {
//                case .success(let upload, _, _):
//
//                    upload.uploadProgress(closure: { (progress) in
//                        print("Upload Progress: \(progress.fractionCompleted)")
//                    })
//
//                    upload.responseJSON { response in
//                        if let value = response.result.value {
//                            print(JSON(value))
//                            observer.onNext(ServiceItem(JSON(value)))
//                            observer.onCompleted()
//                        } else if let error = response.result.error {
//                            observer.onError(error)
//                        }
//                    }
//
//                case .failure(let encodingError):
//                    observer.onError(encodingError)
//                }
//            })
//
//            return Disposables.create()
//        })
//
//    }
  
//    func upload(_ request: CaveRequest, data: Data) -> Observable<CaveRequest> {
//        return Observable.create({ (observer) -> Disposable in
//            
//            Alamofire.upload(multipartFormData: { multipartFormData in
//                multipartFormData.append(data, withName: "map_file", fileName: "file.cave", mimeType: "apllication/fastrope.cave")
//                
//            }, to: API.request(request.id), method: .post, headers: nil, encodingCompletion: { result in
//                switch result {
//                case .success(let upload, _, _):
//                    
//                    upload.uploadProgress(closure: { (progress) in
//                        print("Upload Progress: \(progress.fractionCompleted)")
//                    })
//                    
//                    upload.responseJSON { response in
//                        if let value = response.result.value {
//                            print(JSON(value))
//                            observer.onNext(CaveRequest(JSON(value)))
//                            observer.onCompleted()
//                        } else if let error = response.result.error {
//                            observer.onError(error)
//                        }
//                        
//                    }
//                    
//                case .failure(let encodingError):
//                    observer.onError(encodingError)
//                }
//            })
//            
//            return Disposables.create()
//        })
//        
//    }
    
//    func download(_ url: String) -> Observable<URL> {
//        let fileUrl = self.getSaveFileUrl(fileName: url)
//        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
//            return (fileUrl, [.removePreviousFile, .createIntermediateDirectories])
//        }
//
//        return Observable.create({ (observer) -> Disposable in
//
//            Alamofire.download(url, to: destination)
//                .responseData { data in
//                    if let destinationUrl = data.destinationURL {
//                        observer.onNext(destinationUrl)
//                        observer.onCompleted()
//                    } else if let error = data.error {
//                        observer.onError(error)
//                    }
//
//            }
//
//            return Disposables.create()
//        })
//    }
//
//    func getSaveFileUrl(fileName: String) -> URL {
//        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//        let nameUrl = URL(string: fileName)
//        let fileURL = documentsURL.appendingPathComponent((nameUrl?.lastPathComponent)!)
//        return fileURL;
//    }
    
}
