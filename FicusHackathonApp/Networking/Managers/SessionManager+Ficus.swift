//
//  URLSession+.swift
//
//  Created by Sergey Garazha on 10/16/16.
//  Copyright © 2016. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import SwiftyJSON
import RxAlamofire

fileprivate let logRawHtmlCode = true
fileprivate let logAllRequests = true

public extension Reactive where Base: SessionManager {
  func rightangledRequest(_ method: Alamofire.HTTPMethod,
                   _ url: URLConvertible,
                   parameters: [String: Any]? = nil,
                   encoding: ParameterEncoding = JSONEncoding.default,
                   headers: [String: String]? = nil,
                   log: Bool = false,
                   autohandleUnauthorizedError: Bool = true
    ) -> Observable<(HTTPURLResponse, SwiftyJSON.JSON)> {
    
    var logDetailedResponse = true
    
    return request(method, url, parameters: parameters, encoding: encoding, headers: headers)
      .flatMap { (request: DataRequest) -> Observable<(HTTPURLResponse, SwiftyJSON.JSON)> in
        
        print("[NET] \(request)")
        if log || logAllRequests {
          if let body = request.request?.httpBody, let str = String(data: body, encoding: String.Encoding.utf8) {
            print("[NET] REQUEST BODY: \(str)")
          }
        }
        
        var _request = request
          .response(completionHandler: { (response: DefaultDataResponse) in
            let code = response.response?.statusCode ?? 0
            let url = response.request?.url?.absoluteString ?? "UNKNOWN URL"
            
            if code == 401 {
              NotificationCenter.default.post(name: kUnauthorizedErrorNotificationName, object: nil)
            }
            print("[NET] \(code) \(url)")
          })
        
        if log || logAllRequests {
          _request = _request.responseJSON(completionHandler: { (response: DataResponse<Any>) in
            if response.result.isSuccess {
              let json = SwiftyJSON.JSON(response.result.value ?? "error")
              if json.error == nil {
                print("      BODY JSON: \(json)")
                logDetailedResponse = false
              }
            }
          })
            .responseString(completionHandler: { (response: DataResponse<String>) in
              if logDetailedResponse {
                var str = response.result.value ?? "epmty response string"
                if !logRawHtmlCode && str.contains("<!DOCTYPE html>") {
                  str = "html rendered page"
                }
                print("      BODY STRING: \(str)")
              }
            })
        }
        
        
        return _request.rx
          .responseJSON()
          .map({ (response: HTTPURLResponse, dict: Any) -> (HTTPURLResponse, JSON) in
            
            let json = SwiftyJSON.JSON(dict)
            
            if json["success"].bool == false,
              let message = json["message"].string {
              throw FicusError.custom(message)
            }
            
            
            if let status = json["status"].string,
              status == "error",
              let errors = json["errors"].array,
              let error = errors.first?.string {
              throw FicusError.custom(error)
            }
            
            guard response.statusCode != 401 else {
              let error = FicusError.unathorized(nil)
              if autohandleUnauthorizedError {
                NotificationCenter.default.post(name: kUnauthorizedErrorNotificationName, object: error)
                throw FicusError.silent
              } else {
                throw error
              }
            }
            guard response.statusCode == 200 else { throw FicusError.failureResponse(response.statusCode) }
            
            guard json.error == nil else { throw FicusError.badJSON }
            
            return (response, json)
          })
    }
  }
}
