//
//  AuthManager.swift
//  Rightangled
//
//  Created by  on 09.12.16.
//
//

import Foundation
import SwiftyJSON
import Alamofire
import RxSwift
import RxCocoa

fileprivate enum API {
    static let mandatoryPart = AuthManager.serverURL + AuthManager.pathPrefix
    static let registration = mandatoryPart + "Account/Register"
    static let login = mandatoryPart + "Account/Login"
    //  static let refreshToken = mandatoryPart + "/Authentication/RefreshToken"
    //  static let checkBarcodeAvailable = mandatoryPart + "/Registration/CheckBarcodeAvailable/"
    //  static let address = mandatoryPart + "/Profile/Address"
    //  static let fbLogin = mandatoryPart + "/facebook_sessions"
    //  static let ggLogin = mandatoryPart + "/google_sessions"
    //  static let restorePassword = mandatoryPart + "/auth/passwords"
}

public class AuthManager {
    static let manager = Alamofire.SessionManager.default
}

extension AuthManager {
    
    public static var serverURL: String!
    public static var pathPrefix: String = ""
    private static let db = DisposeBag()
    
    public class func signIn(login: String, password: String) -> Observable<UserFicus> {
        
        let params: [String : Any] = ["Login": login, "Password": password]
        
        return manager.rx
            .rightangledRequest(.post, API.login, parameters: params)
            .map { _, json in
                return UserFicus(json)
        }
    }
    
    public class func signUp(login: String, phoneNumber: String, firstName: String, secondName: String, sex: Int, email: String, password: String) -> Observable<UserFicus> {
        
        let params: [String : Any] = [
            "Login": login,
            "PhoneNumber": phoneNumber,
            "FirstName": firstName,
            "SecondName": secondName,
            "Sex": sex,
            "EMail": email,
            "Password": password
        ]
        
        return manager.rx
            .rightangledRequest(.post, API.registration, parameters: params)
            .map { _, json in
                return UserFicus(json)
        }
    }
    
//    public class func postAddress(country: String, state: String, city: String, address: String, addressTwo: String, postcode: String, phone: String) -> Observable<BaseResponseModel> {
//
//        guard let jwtToket = Settings.jwtToken else {
//            return Observable<BaseResponseModel>.error(RightangledError.custom("jwtToken nil!"))
//        }
//
//        refreshToken()
//
//        let header = ["Authorization": "Bearer " + jwtToket]
//
//        let params = [
//            "country": country,
//            "state": state,
//            "city": city,
//            "address": address,
//            "address2": addressTwo,
//            "zip": postcode,
//            "telephoneNumber": phone
//        ]
//
//        return manager.rx
//            .rightangledRequest(.post, API.address, parameters: params, headers: header)
//            .map { _, json in
//                return BaseResponseModel(json)
//        }
//    }
//
//    public class func getAddress() -> Observable<UserAddress> {
//
//        guard let jwtToket = Settings.jwtToken else {
//            return Observable<UserAddress>.error(RightangledError.custom("jwtToken nil!"))
//        }
//
//        refreshToken()
//
//        let header = ["Authorization": "Bearer " + jwtToket]
//
//        return manager.rx
//            .rightangledRequest(.get, API.address, headers: header)
//            .map { _, json in
//                return UserAddress(json)
//        }
//    }
    
    //  public class func checkBarcodeAvailable(barcode: String) -> Observable<BaseResponseModel> {
    //
    //    return manager.rx
    //      .rightangledRequest(.get, API.checkBarcodeAvailable + barcode)
    //      .map { _, json in
    //        return BaseResponseModel(json)
    //    }
    //  }
    //
    //  public class func signIn(email: String, password: String) -> Observable<AccessToken> {
    //
    //    let params = [
    //      "email": email,
    //      "password": password
    //    ]
    //
    //    return manager.rx
    //      .rightangledRequest(.post, API.login, parameters: params)
    //      .map { _, json in
    //        return AccessToken(json)
    //    }
    //  }
    //
    //  class func postRefreshToken() -> Observable<AccessToken> {
    //
    //    guard let lastTimeRefreshToken = Settings.lastTimeRefreshToken?.toDate() else {
    //      return Observable<AccessToken>.error(RightangledError.custom("lastTimeRefreshToken nil"))
    //    }
    //
    //    if lastTimeRefreshToken.timeIntervalSinceNow <= -260 {
    //      guard let userGuid = Settings.userGuid, let refreshToken = Settings.refreshToken else {
    //        return Observable<AccessToken>.error(RightangledError.custom("userGiud or refreshToken Fail"))
    //      }
    //
    //      let params = [
    //        "userGuid": userGuid,
    //        "refreshToken": refreshToken
    //      ]
    //
    //      return manager.rx
    //        .rightangledRequest(.post, API.refreshToken, parameters: params)
    //        .map { _, json in
    //          return AccessToken(json)
    //      }
    //    } else {
    //      return Observable<AccessToken>.error(RightangledError.jwtTokenValid)
    //    }
    //  }
    //
    //  class func refreshToken() {
    //    AuthManager.postRefreshToken().subscribe(onNext: { (token) in
    //      Settings.jwtToken = token.jwtToken
    //    }, onError: { (error) in
    //
    //      if let e = error as? RightangledError  {
    //        if e.localizedDescriptionError != RightangledError.jwtTokenValid.localizedDescriptionError {
    //          Settings.logout()
    //          UIApplication.shared.keyWindow?.rootViewController = SignInViewController.loadFromStoryboard()
    //        }
    //      }
    //
    //    }).disposed(by: self.db)
    //  }
    //
    //  public class func signIn(fbToken: String) -> Observable<User> {
    //
    //    var params = ["facebook_token": fbToken]
    //
    //    if let pushToken = Settings.pushTokenString {
    //      params["device_token"] = pushToken
    //    }
    //
    //    return manager.rx
    //      .rightangledRequest(.post, API.fbLogin, parameters: params)
    //      .map { _, json in
    //        return User(json["user"])
    //    }
    //  }
    //
    //  public class func signIn(ggToken: String) -> Observable<User> {
    //
    //    var params = ["google_token": ggToken]
    //
    //    if let pushToken = Settings.pushTokenString {
    //      params["device_token"] = pushToken
    //    }
    //
    //    return manager.rx
    //      .rightangledRequest(.post, API.ggLogin, parameters: params)
    //      .map { _, json in
    //        return User(json["user"])
    //    }
    //  }
    //
    //  class func getPasswordRestorationCode(_ email: String) -> Observable<Success> {
    //    return manager.rx
    //      .rightangledRequest(.post, API.restorePassword, parameters: ["email": email])
    //      .map { _, json in
    //        return Success(json)
    //    }
    //  }
    //
    //  class func restorePassword(code: String, password: String, confirmation: String) -> Observable<Void> {
    //    return manager.rx
    //      .rightangledRequest(.put, API.restorePassword, parameters: [
    //        "reset_code": code,
    //        "password": password,
    //        "password_confirmation": confirmation
    //        ])
    //      .map { response, _ in
    //        guard response.statusCode == 200 else {throw RightangledError.unsuccessful}
    //    }
    //  }
    //
    ////  class func deleteUser() -> Observable<Bool> {
    ////    return manager.rx
    ////      .rightangledRequest(.delete, API.registration)
    ////      .map { response, _ in
    ////        return true
    ////    }
    ////  }
    
}
