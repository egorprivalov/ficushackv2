//
//  ContentManager.swift
//  Rightangled
//
//  Created by Anton Pavlov on 09/12/2018.
//  Copyright © 2018 Anton Pavlov. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import RxSwift

fileprivate enum API {
  static let mandatoryPart = ContentNetworkManager.serverURL + ContentNetworkManager.pathPrefix
//  static let serviceCategories = mandatoryPart + "/service_categories"
  
//  static let glossary = mandatoryPart + "/Glossary"
  //    static let request = mandatoryPart + "/requests"
  //    static func repeatRequest(_ requestId: Int, _ serviceItemId: Int) -> String {return mandatoryPart + "/requests/\(requestId)/repeat_request/\(serviceItemId)"}
  //    static func request(_ requestId: Int) -> String {return mandatoryPart + "/requests/\(requestId)"}
  //    static func requestArea(_ requestId: Int) -> String {return mandatoryPart + "/requests/\(requestId)/add_areas_to_request"}
  //    static func requestServiceItem(_ requestId: Int) -> String {return mandatoryPart + "/requests/\(requestId)/add_service_items_to_request"}
  //
  //    static func markResolvedArray(_ requestId: Int) -> String {return mandatoryPart + "/requests/\(requestId)/mark_items_resolved_array"}
  //    static func markResolved(_ requestId: Int, _ serviceItemId: Int) -> String {return mandatoryPart + "/requests/\(requestId)/mark_item_resolved/\(serviceItemId)"}
  //    static func message(_ requestId: Int, _ serviceItemId: Int) -> String {return mandatoryPart + "/requests/\(requestId)/add_item_provider_comment/\(serviceItemId)"}
  //
  //    static func unmarkItem(_ requestId: Int, _ serviceItemId: Int) -> String {return mandatoryPart + "/requests/\(requestId)/unmark_item_resolved/\(serviceItemId)"}
  //
  //    static func deleteArea(_ requestId: Int, _ areaId: Int) -> String {return mandatoryPart + "/requests/\(requestId)/delete_area_from_request/\(areaId)"}
  
}


class ContentNetworkManager {
  static let manager = Alamofire.SessionManager.default
  
  static var serverURL: String!
  static var pathPrefix = ""
  static let db = DisposeBag()
}


extension ContentNetworkManager {
  
//  class func getGlossary() -> Observable<[GlossaryModel]> {
//
//    AuthManager.refreshToken()
//
//    guard let jwtToket = Settings.jwtToken else {
//      return Observable<[GlossaryModel]>.error(RightangledError.custom("jwtToken nil!"))
//    }
//
//    let header = ["Authorization": "Bearer " + jwtToket]
//
//    return manager.rx
//      .rightangledRequest(.get, API.glossary, headers: header).map { _, json in
//        print(json)
//        return json.toArray()
//    }
//  }
  
  //
  //    class func postUnmarkItem(_ caveRequest: CaveRequest, item: ServiceItem) -> Observable<Bool> {
  //
  //        guard let id = item.id else {
  //            return Observable<Bool>.error(RightangledError.custom("id nil"))
  //        }
  //
  //        return manager.rx
  //            .caveRequest(.post, API.unmarkItem(caveRequest.id, id))
  //            .map { _, json in
  //                return true
  //        }
  //    }
  //
  //    class func postCaveRequestRepeat(_ caveRequest: CaveRequest, item: ServiceItem) -> Observable<CaveRequest> {
  //
  //        guard let id = item.id else {
  //            return Observable<CaveRequest>.error(CaveError.custom("id nil"))
  //        }
  //
  //        return manager.rx
  //            .caveRequest(.post, API.repeatRequest(caveRequest.id, id))
  //            .map { _, json in
  //                return CaveRequest(json)
  //        }
  //    }
  //
  //    class func postCaveRequest(_ caveRequest: CaveRequest) -> Observable<CaveRequest> {
  //        return manager.rx
  //            .caveRequest(.post, API.request, parameters: ["areas": caveRequest.toJSON])
  //            .map { _, json in
  //                return CaveRequest(json)
  //        }
  //    }
  //
  //    class func postCaveRequestArea(_ area: [Area], _ caveRequest: CaveRequest) -> Observable<CaveRequest> {
  //        return manager.rx
  //            .caveRequest(.post, API.requestArea(caveRequest.id), parameters: ["areas": area.map { $0.toJSON }])
  //            .map { _, json in
  //                return CaveRequest(json)
  //        }
  //    }
  //
  //    class func postCaveRequestServiceItem(_ serviceItem: [ServiceItem], _ caveRequest: CaveRequest) -> Observable<CaveRequest> {
  //        return manager.rx
  //            .caveRequest(.post, API.requestServiceItem(caveRequest.id), parameters: ["service_items": serviceItem.map { $0.toJSON }])
  //            .map { _, json in
  //                return CaveRequest(json)
  //        }
  //    }
  
  //    class func postCaveRequest(_ caveRequest: CaveRequest) -> Observable<CaveRequest> {
  //        return manager.rx
  //            .caveRequest(.post, API.request, parameters: ["areas": caveRequest.toJSON])
  //            .map { _, json in
  //                return CaveRequest(json)
  //        }
  //    }
  
  //    class func postMarkResolved(_ serviceItem: ServiceItem) -> Observable<ServiceItem> {
  //
  //        guard let id = serviceItem.id, let requestId = serviceItem.requestId else {
  //            return Observable<ServiceItem>.error(CaveError.custom("id nil"))
  //        }
  //
  //        return manager.rx
  //            .caveRequest(.post, API.markResolved(requestId, id), parameters: ["provider_comment": serviceItem.providerComment])
  //            .map { _, json in
  //                return ServiceItem(json)
  //        }
  //    }
  //
  //    class func postMarkResolved(_ serviceItems: [ServiceItem], providerComment: String = "") -> Observable<Bool> {
  //
  //        guard let first = serviceItems.first, let requestId = first.requestId else {
  //            return Observable<Bool>.error(RightangledError.custom("empty array"))
  //        }
  //
  //        let ids = serviceItems.map({ $0.id! })
  //
  //        return manager.rx
  //            .caveRequest(.post, API.markResolvedArray(requestId), parameters: ["service_item_ids": ids, "provider_comment": providerComment])
  //            .map { _, json in
  //                return true
  //        }
  //
  //    }
  //
  //    class func postMessage(_ serviceItem: ServiceItem) -> Observable<ServiceItem> {
  //
  //        guard let id = serviceItem.id, let requestId = serviceItem.requestId else {
  //            return Observable<ServiceItem>.error(CaveError.custom("id nil"))
  //        }
  //
  //        return manager.rx
  //            .caveRequest(.post, API.message(requestId, id), parameters: ["provider_comment": serviceItem.providerComment])
  //            .map { _, json in
  //                return ServiceItem(json)
  //        }
  //    }
  //
  //    class func getCaveRequests(_ pendingOnly: Bool? = nil) -> Observable<[CaveRequest]> {
  //
  //        var json = [String: Any]()
  //        if let pendingOnly = pendingOnly {
  //            json["pending_only"] = String(pendingOnly)
  //        }
  //
  //        return manager.rx
  //            .caveRequest(.get, API.request, parameters: json, encoding: URLEncoding.default)
  //            .map { _, json in
  //                return json.toArray()
  //        }
  //    }
  //
  //    class func getCaveRequest(_ id: Int) -> Observable<CaveRequest> {
  //        return manager.rx
  //            .caveRequest(.get, API.request(id))
  //            .map { _, json in
  //                return CaveRequest(json)
  //        }
  //    }
  //
  //    class func deleteArea(_ area: Area) -> Observable<Success> {
  //
  //        guard let id = area.id, let requestId = area.requestId else {
  //            return Observable<Success>.error(RightangledError.custom("id nil"))
  //        }
  //
  //        return manager.rx
  //            .caveRequest(.delete, API.deleteArea(requestId, id))
  //            .map { _, json in
  //                return Success(json)
  //        }
  //    }
  //
}
