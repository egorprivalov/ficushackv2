//
//  Arrayable.swift
//  Cave
//
//  Created by Anton Pavlov on 09/12/2018.
//  Copyright © 2018 Anton Pavlov. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol Arrayable {
    init(_ json: JSON) throws
}

extension JSON  {
    func toArray<E: Arrayable>() -> [E] {
        var res: [E] = [E]()
        if let ar = self.array {
            for i in ar {
                if let new = try? E(i) {
                    res.append(new)
                }
            }
        }
        
        return res
    }
}

