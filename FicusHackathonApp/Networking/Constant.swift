//
//  Constant.swift
//  Cave
//
//  Created by Anton Pavlov on 07/12/2018.
//  Copyright © 2018 Anton Pavlov. All rights reserved.
//

import Foundation
import SceneKit
import ARKit

let testing = false


let testServerURL = "http://193.124.58.65/hackathon/Api/"
let prodServerURL = "http://193.124.58.65/hackathon/Api/"

var date = Date()
var currentCalendar = Calendar.current

//let requestPathPrefix = "/api/v1"

var DEBUG: Bool {
    if testing {return true}
    
    let dic = ProcessInfo.processInfo.environment
    return dic["DEBUG"] != nil
}

let baseServerURL = testing ? testServerURL : prodServerURL

let glossaryHTMLStyle = """
<style>
p {
box-sizing: border-box;
color: rgba(0, 0, 0, 0.87);
display: block;
font-family: Poppins, Arial, sans-serif ;
font-size: 20pt;
font-weight: 400;
line-height: 22.5pt;
list-style-type: none;
margin-block-end: 15px;
margin-block-start: 15px;
margin-inline-end: 0px;
margin-inline-start: 0px;
text-align: justify;
text-size-adjust: 100%;
width: 100%;
padding: 5px 10px;
}
</style>
"""
