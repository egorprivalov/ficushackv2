//
//  AppDelegate.swift
//  Ficus
//
//  Created by MacBook on 08/06/2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        AuthManager.serverURL = baseServerURL
        ContentNetworkManager.serverURL = baseServerURL
        
        let rootViewController = LoadingViewController()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: rootViewController)
        window?.makeKeyAndVisible()
        return true
    }
}

