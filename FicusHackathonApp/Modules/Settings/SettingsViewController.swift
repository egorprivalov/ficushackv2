//
//  SettingsViewController.swift
//  Ficus
//
//  Created by MacBook on 08/06/2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    private lazy var buttob = UIButton(target: self, selector: #selector(logOut), title: "Log out", size: 100, color: UIColor.ficusGreen)
    
    override func loadView() {
        view = UIView().background(.white)
       
        view.addSubview(buttob)
        
        activateConstraints(
            buttob.center()
        )
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Настройки"
        self.view.background(.red)
    }
    
    @objc private func logOut() {
        Settings.logout()
    }

}
