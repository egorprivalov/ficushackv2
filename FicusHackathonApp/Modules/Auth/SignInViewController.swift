//
//  SignInViewController.swift
//  Ficus
//
//  Created by Egor Privalov on 08.06.2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SignInViewController: UIViewController {
    
    private lazy var svContent = UIScrollView()
    private lazy var vContent = UIView()
    private lazy var lblAutentification = UILabel().lines(0).color(UIColor("#2CB8DD")).font(UIFont.systemFont(ofSize: 30, weight: .bold)).alignment(.left).text("Авторизация")
    private lazy var btnSignUp = UIButton(target: self, selector: #selector(btnSignUp_Action), title: "Регистрация", color: UIColor.blue).font(UIFont.systemFont(ofSize: 12, weight: .regular))
    
    private lazy var vLogin: AuthTextField = {
        let login = AuthTextField()
        login.lblTitle.text = "Логин"
        login.txtTitle.placeholder = "Логин"
        login.txtTitle.returnKeyType = .next
        login.txtTitle.keyboardType = .numberPad
        return login
    }()
    
    private lazy var vPassword: AuthTextField = {
        let password = AuthTextField()
        password.lblTitle.text = "Пароль"
        password.txtTitle.placeholder = "Пароль"
        password.txtTitle.returnKeyType = .done
        password.txtTitle.isSecureTextEntry = true
        return password
    }()
    
    private lazy var btnSignIn: UIButton = {
        let btn = UIButton().title("Вход").background(UIColor("#2CB8DD"))
        btn.setTitleColor(.white, for: .normal)
        btn.layer.cornerRadius = 15
        return btn
    }()
    
    private let db = DisposeBag()
    
    override func loadView() {
        print("HEH")
        view = UIView().background(.white)
        vContent.addSubviews(lblAutentification, vLogin, vPassword, btnSignIn, btnSignUp)
        svContent.addSubview(vContent)
        view.addSubview(svContent)
        
        activateConstraints(
            svContent.edges(),
            vContent.edges().width(of: svContent),
            lblAutentification.top(100).leading(15).trailing(),
            vLogin.pinTop(10 ,to: lblAutentification).leading().trailing(),
            vPassword.pinTop(to: vLogin).leading().trailing(),
            btnSignIn.pinTop(30, to: vPassword).leading(100).trailing(100).height(54),
            btnSignUp.pinTop(20, to: btnSignIn).centerX().bottom(50)
        )
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Авторизация"
        self.hideKeyboard()
        setDelegates()
        addObservers()
        addTargets()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setDelegates() {
        vLogin.txtTitle.delegate = self
        vPassword.txtTitle.delegate = self
    }
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func addTargets() {
        btnSignIn.addTarget(self, action: #selector(btnSignIn_Action), for: .touchUpInside)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 && vPassword.txtTitle.isEditing {
                self.view.frame.origin.y -= keyboardSize.height / 2
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc private func btnSignIn_Action() {
        
        guard let login = vLogin.txtTitle.text, let password = vPassword.txtTitle.text else { return }
        
        if login.isEmpty {
            vLogin.vBack.shake()
            return
        }
        
        if password.isEmpty {
            vPassword.vBack.shake()
            return
        }
        
        // TODO: POST запрос на авторизацию.
        AuthManager.signIn(login: login, password: password).subscribe(onNext: { (user) in
            
            Settings.phoneNumber = user.phoneNumber
            Settings.email = user.eMail
            Settings.password = password
            Settings.isUserLoggedIn = true
            Settings.shared.user = user
            
            let vc = CustomTabBarController()
            UIApplication.shared.keyWindow?.rootViewController = vc
        }, onError: { (error) in
            print(error.localizedDescription)
            Settings.logout()
        }).disposed(by: db)
    }
    
    @objc private func btnSignUp_Action() {
        let vc = SignUpViewController()
        let nav: UINavigationController = UINavigationController(rootViewController: vc)
        self.present(nav, animated: true, completion: nil)
    }
    
}

extension SignInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == vLogin.txtTitle {
            vPassword.txtTitle.becomeFirstResponder()
        } else if textField == vPassword.txtTitle {
            textField.resignFirstResponder()
        }
        return false
    }
}

