//
//  SignUpViewController.swift
//  Ficus
//
//  Created by Egor Privalov on 08.06.2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SignUpViewController: UIViewController {
    
    private lazy var svContent = UIScrollView()
    private lazy var vContent = UIView()
    private lazy var vFirstName = AuthTextField("Имя", placeholder: "Имя", returnKeyType: .next)
    private lazy var vLastName = AuthTextField("Фамилия", placeholder: "Фамилия", returnKeyType: .next)
    private lazy var vGender = AuthTextField("Пол", placeholder: "Пол", returnKeyType: .next)
    private lazy var vPhoneNumber = AuthTextField("Телефон", placeholder: "+7-(XXX)-XXX-XX-XX", returnKeyType: .next, keyboardType: .numberPad)
    private lazy var vMail = AuthTextField("Почта", placeholder: "Email", returnKeyType: .next, keyboardType: .emailAddress)
    private lazy var vPassword = AuthTextField("Пароль", placeholder: "Пароль", returnKeyType: .next, isSecureTextEntry: true)
    private lazy var vConfirmPassword = AuthTextField("Повторите пароль", placeholder: "Повторите пароль", returnKeyType: .done, isSecureTextEntry: true)
    
    private let db = DisposeBag()
    private var pickOption = ["Муж.", "Жен."]
    
    override func loadView() {
        
        view = UIView().background(.white)
        vContent.addSubviews(vFirstName, vLastName, vGender, vMail, vPhoneNumber, vPassword, vConfirmPassword)
        svContent.addSubview(vContent)
        view.addSubview(svContent)
        
        activateConstraints(
            svContent.edges(),
            vContent.edges().width(of: svContent),
            vFirstName.dockTop(),
            vLastName.pinTop(to: vFirstName).leading().trailing(),
            vGender.pinTop(to: vLastName).leading().width(200),
            vPhoneNumber.pinTop(to: vGender).leading().trailing(),
            vMail.pinTop(to: vPhoneNumber).leading().trailing(),
            vPassword.pinTop(to: vMail).leading().trailing(),
            vConfirmPassword.pinTop(to: vPassword).leading().trailing().bottom(20)
        )
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Регистрация"
        self.hideKeyboard()
        setDelegates()
        addNavigationItems()
        addObservers()
        addPicker()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setDelegates() {
        vFirstName.txtTitle.delegate = self
        vLastName.txtTitle.delegate = self
        vGender.txtTitle.delegate = self
        vPhoneNumber.txtTitle.delegate = self
        vMail.txtTitle.delegate = self
        vPassword.txtTitle.delegate = self
        vConfirmPassword.txtTitle.delegate = self
    }
    
    private func addNavigationItems() {
        let cancel = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(btnCancel_Action))
        self.navigationItem.leftBarButtonItem = cancel
        let signUp = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(btnSignUp_Action))
        self.navigationItem.rightBarButtonItem = signUp
    }
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func addPicker() {
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        vGender.txtTitle.inputView = pickerView
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor("#2CB8DD")
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        vGender.txtTitle.inputAccessoryView = toolBar
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 && !vFirstName.txtTitle.isEditing
            && !vLastName.txtTitle.isEditing && !vGender.txtTitle.isEditing {
                self.view.frame.origin.y -= keyboardSize.height - 10
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc private func btnCancel_Action() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func doneClick() {
        vGender.txtTitle.resignFirstResponder()
    }
    @objc private func cancelClick() {
        vGender.txtTitle.resignFirstResponder()
    }
    
    @objc private func btnSignUp_Action() {
        
        guard
            let firstName = vFirstName.txtTitle.text, let lastName = vLastName.txtTitle.text, let gender = vGender.txtTitle.text,
            let phoneNumber = vPhoneNumber.txtTitle.text, let email = vMail.txtTitle.text, let password = vPassword.txtTitle.text,
            let confirmPassword = vConfirmPassword.txtTitle.text else { return }
        
        if firstName.isEmpty {
            vFirstName.vBack.shake()
            return
        }
        
        if lastName.isEmpty {
            vLastName.vBack.shake()
            return
        }
        
        if gender.isEmpty {
            vGender.vBack.shake()
            return
        }
        
        if phoneNumber.isEmpty {
            vPhoneNumber.vBack.shake()
            return
        }
        
        if email.isEmpty {
            vMail.vBack.shake()
            return
        }
        
        if password.isEmpty {
            vPassword.vBack.shake()
            return
        }
        
        if confirmPassword.isEmpty || confirmPassword != password {
            vConfirmPassword.vBack.shake()
            return
        }
        
        var sex: Int {
            if gender == "Муж." {
                return 1
            } else {
                return 2
            }
        }
        
        // TODO: POST запрос на регистрацию.
        
        AuthManager.signUp(login: phoneNumber, phoneNumber: phoneNumber, firstName: firstName, secondName: lastName, sex: sex, email: email, password: password).subscribe(onNext: { (user) in
            
            Settings.email = user.eMail
            Settings.phoneNumber = user.phoneNumber
            Settings.password = password
            Settings.isUserLoggedIn = true
            Settings.shared.user = user
            
            let vc = CustomTabBarController()
            UIApplication.shared.keyWindow?.rootViewController = vc
            
        }, onError: { (error) in
            let alert = UIAlertController(title: "Alert", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }).disposed(by: db)
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == vFirstName.txtTitle {
            vLastName.txtTitle.becomeFirstResponder()
        } else if textField == vLastName.txtTitle {
            vGender.txtTitle.becomeFirstResponder()
        } else if textField == vGender.txtTitle {
            vPhoneNumber.txtTitle.becomeFirstResponder()
        } else if textField == vPhoneNumber.txtTitle {
            vMail.txtTitle.becomeFirstResponder()
        } else if textField == vMail.txtTitle {
            vPassword.txtTitle.becomeFirstResponder()
        } else if textField == vPassword.txtTitle {
            vConfirmPassword.becomeFirstResponder()
        } else if textField == vConfirmPassword.txtTitle {
            textField.resignFirstResponder()
        }
        return false
    }
}

extension SignUpViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
}

extension SignUpViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       vGender.txtTitle.text = pickOption[row]
    }
}


