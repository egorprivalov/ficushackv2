//
//  ProfileViewController.swift
//  Ficus
//
//  Created by MacBook on 08/06/2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    struct Section {
        var title: String
        var rows: [Row]
    }
    
    struct Row {
        var model: ProfileModel
    }
    
    var sections: [Section] =
    [
        Section(title: "Name", rows: [Row(model: ProfileModel(name: "Egor", lastName: "Privalov", phoneNumber: "+7-(999)-699-7000"))])
    ]
    
    lazy var tableProfile: UITableView = {
        let table = UITableView()
        table.delegate = self
        table.dataSource = self
        table.separatorStyle = .none
        table.register(ProfileTableViewCell.self, forCellReuseIdentifier: ProfileTableViewCell.reuseIdentifierCell)
        return table
    }()
    
    override func loadView() {
        
        view = UIView().background(.white)
        view.addSubview(tableProfile)
        
        activateConstraints(
            tableProfile.edges()
        )
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Профиль"
        self.view.background(.yellow)
    }
}

extension ProfileViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileTableViewCell.reuseIdentifierCell, for: indexPath) as? ProfileTableViewCell else { return UITableViewCell() }
            cell.configure(with: sections[indexPath.section].rows[indexPath.row].model)
            return cell
        default:
            return UITableViewCell()
        }
        //        switch indexPath.section {
        //        case 0:
        //            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileTableViewCell.reuseIdentifierCell, for: indexPath) as? ProfileTableViewCell else { return UITableViewCell() }
        //
        //            cell.configure(with: ProfileModel(name: "Egor", lastName: "Privalov"))
        //
        //            return cell
        //        default:
        //            return UITableViewCell().background(.yellow)
        //        }
    }
}

extension ProfileViewController: UITableViewDelegate {
    
}
