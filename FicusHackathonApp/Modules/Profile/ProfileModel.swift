//
//  ProfileModel.swift
//  Ficus
//
//  Created by Egor Privalov on 08.06.2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import Foundation

struct ProfileModel {
    var name: String?
    var lastName: String?
    var phoneNumber: String?
}
