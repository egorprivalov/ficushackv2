//
//  ProfileTableViewCell.swift
//  Ficus
//
//  Created by Egor Privalov on 08.06.2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    
    private lazy var lblName = UILabel().color(.black).font(UIFont.systemFont(ofSize: 17, weight: .medium))
    private lazy var lblLastName = UILabel().color(.black).font(UIFont.systemFont(ofSize: 17, weight: .regular))
    private lazy var vSeparator = UIView().background(.white)
    private lazy var imgPhoto = UIImageView().clip(true).corners(30)
    
    private lazy var vBack: UIView = {
        let view = UIView().background(UIColor("#f7f7f7"))
        view.layer.cornerRadius = 15
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        self.background(.white)
        vBack.addSubviews(imgPhoto, lblName, lblLastName, vSeparator)
        self.addSubview(vBack)
        
        activateConstraints(
            vBack.edges(15),
            imgPhoto.top(10).leading(10).bottom(10).height(60).width(60),
            lblName.top(10).pinLeft(10, to: imgPhoto).trailing(10),
            vSeparator.pinTop(10, to: lblName).pinLeft(10, to: imgPhoto).trailing().height(1),
            lblLastName.pinTop(10, to: vSeparator).pinLeft(10, to: imgPhoto).trailing(10).bottom(10)
        )
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        lblName.text = ""
        lblLastName.text = ""
        imgPhoto.image = nil
    }
    
    func configure(with model: ProfileModel) {
        lblName.text = model.name! + " " + model.lastName!
        lblLastName.text = model.phoneNumber
        imgPhoto.backgroundColor = .red
    }
}
