//
//  CreateOrderView.swift
//  Ficus
//
//  Created by MacBook on 08/06/2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit

protocol CreateOrderViewDelegate: class {
    func findDidClick(fromPlace: String, toPlace: String)
}

class CreateOrderView: UIView {
    
    weak var delegate: CreateOrderViewDelegate?
    
    private lazy var vChoose = UIView()
    private lazy var btnDriver = UIButton(target: self, selector: #selector(btnDriver_Click), title: "Я водитель", size: 16.0, color: .ficusBlue).background(.ficusLightGray).corners(10)
    private lazy var btnPassenger = UIButton(target: self, selector: #selector(btnPassenger_Click), title: "Я пассажир", size: 16.0, color: .ficusGray).corners(10)
    
    private lazy var lblRoute = UILabel("Маршрут").color(.ficusGray).font(.systemFont(ofSize: 13.0))
    
    private lazy var vRouteContainer = UIView(.ficusLightGray).corners(15)
    private lazy var imgFrom = UIImageView(image: UIImage(named: "")).corners(22).background(.ficusGray)
    private lazy var txtFrom = UITextField().placeholder("Откуда")
    private lazy var vSeparator = UIView(.ficusGray).alpha(0.3)
    private lazy var imgTo = UIImageView(image: UIImage(named: "")).corners(22).background(.ficusGray)
    private lazy var txtTo = UITextField().placeholder("Куда")
    
    private lazy var vTimeContainer = UIView()
    private lazy var lblTimeStart = UILabel("Время отправления").color(.ficusGray).font(.systemFont(ofSize: 13.0))
    private lazy var lblTimeFrom = UILabel("от").color(.ficusGray).font(.systemFont(ofSize: 15.0)).alignment(.right)
    private lazy var btnTimeFrom = UIButton(target: self, selector: #selector(btnTimeFrom_Click), title: "18:30", size: 16.0, color: .black).corners(15).background(.ficusLightGray)
    private lazy var lblTimeTo = UILabel("до").color(.ficusGray).font(.systemFont(ofSize: 15.0)).alignment(.right)
    private lazy var btnTimeTo = UIButton(target: self, selector: #selector(btnTimeTo_Click), title: "18:30", size: 16.0, color: .black).corners(15).background(.ficusLightGray)

    private lazy var btnSearch = UIButton(target: self, selector: #selector(btnSearch_Click), title: "Поиск", size: 18.0, color: .white).background(.ficusBlue).corners(15)

    init() {
        super.init(frame: .zero)
        
        self.background(.white)
        self.addSubviews(vChoose, lblRoute, vRouteContainer, vTimeContainer, btnSearch)
        vChoose.addSubviews(btnDriver, btnPassenger)
        vRouteContainer.addSubviews(imgFrom, txtFrom, vSeparator, imgTo, txtTo)
        vTimeContainer.addSubviews(lblTimeStart, lblTimeFrom, btnTimeFrom, lblTimeTo, btnTimeTo)
        
        activateConstraints(
            vChoose.dockTop(20),
            btnDriver.top().leading().width(150).height(40).bottom(),
            btnPassenger.top().trailing().width(150).height(40),
            
            lblRoute.pinTop(10, to: vChoose).leading(22),
            vRouteContainer.pinTop(5, to: lblRoute).leading(14).trailing(14),
            imgFrom.leading(5).top(5).width(44).height(44),
            txtFrom.pinLeft(10, to: imgFrom).centerY(to: imgFrom).trailing(5),
            vSeparator.pinTop(5, to: imgFrom).leading().trailing().height(1),
            imgTo.leading(5).pinTop(5, to: vSeparator).width(44).height(44).bottom(5),
            txtTo.pinLeft(10, to: imgTo).centerY(to: imgTo).trailing(5),
            
            vTimeContainer.pinTop(5, to: vRouteContainer).leading().trailing(),
            lblTimeStart.top(5).leading(22),
            btnTimeFrom.leading(50).height(44).width(88).pinTop(5, to: lblTimeStart).bottom(5),
            lblTimeFrom.pinRight(5, to: btnTimeFrom).centerY(to: btnTimeFrom),
            lblTimeTo.pinRight(5, to: btnTimeTo).centerY(to: btnTimeTo),
            btnTimeTo.pinLeft(50, to: btnTimeFrom).centerY(to: btnTimeFrom).height(44).width(88),
            
            btnSearch.pinTop(20, to: vTimeContainer).leading(40).trailing(40).height(50)
        )
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func btnDriver_Click(_ sender: UIButton) {
        sender.background(.ficusLightGray)
        sender.setTitleColor(.ficusBlue, for: .normal)
        btnPassenger.background(.clear)
        btnPassenger.setTitleColor(.ficusGray, for: .normal)
    }
    
    @objc
    private func btnPassenger_Click(_ sender: UIButton) {
        sender.background(.ficusLightGray)
        sender.setTitleColor(.ficusBlue, for: .normal)
        btnDriver.background(.clear)
        btnDriver.setTitleColor(.ficusGray, for: .normal)
    }
    
    @objc
    private func btnTimeFrom_Click(_ sender: UIButton) {
        
    }
    
    @objc
    private func btnTimeTo_Click(_ sender: UIButton) {
        
    }
    
    @objc
    private func btnSearch_Click(_ sender: UIButton) {
        delegate?.findDidClick()
    }
}
