//
//  AutoDetailView.swift
//  Ficus
//
//  Created by MacBook on 08/06/2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit

class AutoDetailView: UIView {
    
    private var height: CGFloat {
        return UIScreen.main.bounds.height - 300.0
    }
    var fuel: Int = 0 {
        didSet {
            var color = UIColor.white
            if fuel > 75 {
                color = UIColor.ficusGreen
            } else if fuel > 25 {
                color = UIColor.orange
            } else {
                color = UIColor.red
            }
            let attr1 = NSAttributedString(string: "БАК: ", attributes: [NSAttributedString.Key.foregroundColor: UIColor.ficusGray])
            let attr2 = NSAttributedString(string: "\(fuel)%", attributes: [NSAttributedString.Key.foregroundColor: color])
            let attributedText = NSMutableAttributedString(attributedString: attr1)
            attributedText.append(attr2)
            lblFuel.attributedText = attributedText
        }
    }
    
    private lazy var vLine = UIView().background(.black).alpha(0.3).corners(10)
    private lazy var lblName = UILabel("HYUNDAI SOLARIS").font(.boldSystemFont(ofSize: 16.0))
    private lazy var lblNumber = UILabel("O278TM 777").font(.systemFont(ofSize: 12.0)).color(UIColor("#777777"))
    private lazy var lblFuel = UILabel("123").font(.systemFont(ofSize: 13))
    
    private lazy var imgLogo = UIImageView(image: UIImage(named: "")).background(.lightGray).corners(22)
    private lazy var lblLogo = UILabel("Делимобиль").color(.ficusGray)
    
    private lazy var lblPrice = UILabel("8.00 Руб/Мин").font(.systemFont(ofSize: 16.0))
    private lazy var lblWaitPrice = UILabel("ОЖИДАНИЕ: 2.5 Руб/Мин").font(.systemFont(ofSize: 12.0))
    
    private lazy var btnBooking = UIButton(target: self, selector: #selector(btnBooking_Click), title: "БРОНИРУЮ", size: 18.0, color: .white).background(.ficusBlue).corners(10)
    
    init() {
        super.init(frame: .zero)
        self.background(.white).corners(10)
        self.addSubviews(vLine, lblName, lblNumber, lblFuel, imgLogo, lblLogo, lblPrice, lblWaitPrice, btnBooking)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(pullAnimation))
        self.addGestureRecognizer(panGesture)
                
        activateConstraints(
            vLine.top(5).centerX().width(150).height(8),
            lblName.top(20).leading(20),
            lblNumber.pinTop(5, to: lblName).leading(20),
            lblFuel.pinTop(5, to: lblName).trailing(20),
            
            imgLogo.pinTop(5, to: lblNumber).leading(20).width(44).height(44),
            lblLogo.pinLeft(5, to: imgLogo).centerY(to: imgLogo),
            
            lblPrice.pinTop(10, to: imgLogo).leading(20),
            lblWaitPrice.pinTop(10, to: lblPrice).leading(20),
            btnBooking.pinTop(10, to: imgLogo).width(200).trailing(20).height(50)
        )
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func pullAnimation(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self)
        
        if translation.y >= 0 {
            self.frame.origin.y = height + translation.y
        }
        
        if recognizer.state == .ended {
            let velocity = recognizer.velocity(in: self)
            if velocity.y >= 100 {
                self.isHidden = true
            } else {
                UIView.animate(withDuration: 0.3) {
                    self.frame.origin = CGPoint(x: 0, y: self.height)
                }
            }
        }
        
    }
    
    @objc
    private func btnBooking_Click(_ sender: UIButton) {
        
    }
}
