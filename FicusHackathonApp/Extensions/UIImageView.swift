//
//  UIImageView.swift
//  Cave
//
//  Created by Anton Pavlov on 19/11/2018.
//  Copyright © 2018 Anton Pavlov. All rights reserved.
//

import UIKit


extension UIImageView {
    override open func awakeFromNib() {
        super.awakeFromNib()
        tintColorDidChange()
    }
}
