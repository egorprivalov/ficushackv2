//
//  UIView+Extensions.swift
//  rightangled
//
//  Created by Egor Privalov on 04/02/2019.
//  Copyright © 2019 rightangled. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    func round() {
        layer.cornerRadius = frame.height / 2
    }
}
