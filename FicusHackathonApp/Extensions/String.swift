//
//  String.swift
//  Cave
//
//  Created by Egor Privalov on 07/12/2018.
//  Copyright © 2018 Anton Pavlov. All rights reserved.
//

import Foundation

extension String {
    
    func isEmail() -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,64}$",
                                             options: [.caseInsensitive])
        
        let trimmed = self.trimmingCharacters(in: .whitespaces)
        
        return regex.firstMatch(in: trimmed, options:[],
                                range: NSMakeRange(0, trimmed.utf16.count)) != nil
    }
    
}
