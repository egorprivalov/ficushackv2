
import UIKit

//private var kTextFieldEventAction: Int = 0

extension UITextField {
	
	private static var kTextFieldEventAction: Int = 0
	
	typealias TextFieldAction = () -> ()
	
    var action: TextFieldAction? {
        get {
			let action = objc_getAssociatedObject(self, &UITextField.kTextFieldEventAction) as? TextFieldAction
            return action
        }
        set {
			objc_setAssociatedObject(self, &UITextField.kTextFieldEventAction, newValue as AnyObject, .OBJC_ASSOCIATION_COPY_NONATOMIC)
        }
    }
    
    // MARK: - UIButton + Closure
    
    @objc
    private func txt_changed(_ sender: UITextField) {
        action?()
    }
    
    // MARK: - Experimental
    
    /**
     - Note: *Action* can be possibly renamed to *Changed*
     - Warning:
     If using UITextField as property in this case property must by *lazy*
     */
    func action(_ action: @escaping () -> ()) -> UITextField {
        self.action = action
        self.addTarget(self, action: #selector(txt_changed), for: .editingChanged)
        return self
    }
}
