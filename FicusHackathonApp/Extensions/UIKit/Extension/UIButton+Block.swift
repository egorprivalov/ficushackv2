

import UIKit

//private var kButtonEventAction: Int = 0

extension UIButton {
	private static var kButtonEventAction: Int = 0
	
	typealias ButtonAction = () -> ()
	
	private/*(set) */var action: ButtonAction? {
        get {
			let action = objc_getAssociatedObject(self, &UIButton.kButtonEventAction) as? ButtonAction
            return action
        }
        set {
			objc_setAssociatedObject(self, &UIButton.kButtonEventAction, newValue as AnyObject, .OBJC_ASSOCIATION_COPY_NONATOMIC)
        }
    }
    
    // MARK: - UIButton + Closure
    
    @objc
    private func btnClick_Action(_ sender: UIButton) {
        action?()
    }

    // MARK: - Experimental
    
    /**
     - Warning:
     If using UIButton as property in this case property must by *lazy*
     */
    func action(_ action: @escaping () -> ()) -> UIButton {
        self.action = action
        self.addTarget(self, action: #selector(btnClick_Action(_:)), for: .touchUpInside)
        return self
    }
}
