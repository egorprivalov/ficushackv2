//
//  UIColor+ManUtd.swift
//  manutdone
//
//  Created by Egor Privalov on 02/06/2019.
//  Copyright © 2019 eprivalov. All rights reserved.
//

import UIKit

extension UIColor {
    static let ficusBlue = UIColor("#2CB8DD")
    static let ficusGray = UIColor("#777777")
    static let ficusGreen = UIColor("#7ED321")
    static let ficusLightGray = UIColor("#F7F7F7")
}
