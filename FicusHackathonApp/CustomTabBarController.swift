//
//  CustomTabBarController.swift
//  Ficus
//
//  Created by MacBook on 08/06/2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mapVC = MapViewController()
        let mapNavVC = UINavigationController(rootViewController: mapVC)
        mapVC.tabBarItem = UITabBarItem(title: "Карта", image: UIImage(named: "icon_map"), tag: 0)
        
        let profileVC = ProfileViewController()
        let profileNavVC = UINavigationController(rootViewController: profileVC)
        profileVC.tabBarItem = UITabBarItem(title: "Профиль", image: UIImage(named: "icon_profile"), tag: 1)
        
        let settingsVC = SettingsViewController()
        let settingsNavVC = UINavigationController(rootViewController: settingsVC)
        settingsVC.tabBarItem = UITabBarItem(title: "Настройки", image: UIImage(named: "icon_settings"), tag: 2)
        
        self.tabBar.tintColor = .ficusBlue
        self.viewControllers = [mapNavVC, profileNavVC, settingsNavVC]
    }

}
