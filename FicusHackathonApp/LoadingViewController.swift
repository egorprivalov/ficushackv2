//
//  LoadingViewController.swift
//  Ficus
//
//  Created by Egor Privalov on 08.06.2019.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LoadingViewController: UIViewController {
    
    private let db = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Settings.isUserLoggedIn == true {
            guard let email = Settings.email, let password = Settings.password, let phoneNumber = Settings.phoneNumber else {
                Settings.logout()
                return
            }
            
            AuthManager.signIn(login: phoneNumber, password: password).subscribe(onNext: { (user) in
                
                Settings.email = email
                Settings.phoneNumber = user.phoneNumber
                Settings.password = password
                Settings.isUserLoggedIn = true
                Settings.shared.user = user
                
                let vc = CustomTabBarController()
                UIApplication.shared.keyWindow?.rootViewController = vc
                
            }, onError: { (error) in
                Settings.logout()
                let vc = SignInViewController()
                UIApplication.shared.keyWindow?.rootViewController = vc
                if let er = error as? FicusError {
                    print(error)
                }
            }).disposed(by: db)
        } else {
            Settings.logout()
            let vc = SignInViewController()
            UIApplication.shared.keyWindow?.rootViewController = vc
//            UIApplication.shared.keyWindow?.rootViewController = SignInViewController()
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.background(.white)
    }

}
